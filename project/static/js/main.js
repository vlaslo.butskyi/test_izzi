let form = document.getElementById('my-form')[0];

form.addEventListener('sumbit', function (){alert('hello');});

;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
  form.addEventListener(eventName, preventDefaults, false);
});

function preventDefaults (e) {
  e.preventDefault();
  e.stopPropagation();
}

['dragenter', 'dragover'].forEach(eventName => {
  form.addEventListener(eventName, highlight, false);
});

['dragleave', 'drop'].forEach(eventName => {
  form.addEventListener(eventName, unhighlight, false);
});

function highlight(e) {
  form.classList.add('highlight');
}

function unhighlight(e) {
  form.classList.remove('highlight');
}

form.addEventListener('drop', handleDrop, false);

function handleDrop(e) {
  let dt = e.dataTransfer;
  let files = dt.files;

  let fileInput = document.getElementById('file-input');
  console.log(fileInput);

  handleFiles(files);
}

function handleFiles(files) {
  ([...files]).forEach(uploadFile);
}

function uploadFile(file) {
  let url = '/upload-file';
  let formData = new FormData();

  formData.append('file', file);

  fetch(url, {
    method: 'POST',
    body: formData
  })
      .then(response => response.json())
      .then((data) => {
        let resultMessage = document.getElementById('result-message');
        resultMessage.style.display = 'unset';
        writeText(data.message, "result-message");
      })
      .catch(
          error => console.log(error)
      );
}

function writeText(text, id, position = 1) {
  let destination = document.getElementById(id);

  destination.innerHTML = text.substring(0, position);
  if (position <= text.length){
    setTimeout(function (){
      writeText(text, id, position + 1);
    }, 50);
  }
}

writeText(
    "Завантажте, будь ласка, файл формату CSV для завантаження інформації про користувачів в базу даних",
    "typedtext"
)
