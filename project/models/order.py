from project import db


class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    creation_date = db.Column(db.DateTime())
    product = db.Column(db.String(128))

    def __init__(self, creation_date, product):
        self.creation_date = creation_date
        self.product = product

    def __repr__(self):
        return f'<City {self.name}>'
