from project import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(32), nullable=False)
    last_name = db.Column(db.String(32), nullable=False)
    birth_date = db.Column(db.DateTime(), nullable=False)
    registration_date = db.Column(db.DateTime(), index=True, nullable=False)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'))
    order = db.relationship("Order", uselist=False, backref="order")

    def __init__(self, first_name, last_name, birth_date, registration_date):
        self.first_name = first_name
        self.last_name = last_name
        self.birth_date = birth_date
        self.registration_date = registration_date

    def __repr__(self):
        return f'<User {self.id}>'

    def commit(self):
        db.session.add(self)
        db.session.commit()
        return self
