from . import errors_bp


@errors_bp.app_errorhandler(404)
def page_not_found(error):
    return "I'm sorry, but this page in not exist", 404


@errors_bp.app_errorhandler(500)
def page_not_found(error):
    return "some went wrong, " \
           "please try again in some time", 500
