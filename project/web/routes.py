from . import web_bp
from ..tools import export_file_to_db, \
    NotFoundTitles, WrongData

from flask import render_template, request


@web_bp.route('/', methods=['GET'])
def index():
    return render_template('main.html')


@web_bp.route('/upload-file', methods=['POST'])
def upload_file():
    try:
        fstring = request.files["file"]
        separators = ';,'

        export_file_to_db(fstring, separators, 'rb')
    except NotFoundTitles:
        return {
            'message': 'Не знайдені заголовки колонок'
        }
    except WrongData:
        return {
            'message': 'Файл містить помилкові дані'
        }
    except Exception:
        import traceback
        traceback.print_exc()
        return {
            'message': 'Щось пішло не так'
        }
    else:
        return {
            'message': 'Файл успішно завантажений'
        }
