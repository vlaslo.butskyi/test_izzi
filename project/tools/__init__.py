from .csv_parser import *
from sqlalchemy.exc import DataError


def export_file_to_db(file, separator, read_mode):
    must_have_titles = ['FirstName', 'LastName', 'BirthDate', 'RegistrationDate']

    input_data = CSVReader(file, separator, must_have_titles, read_mode=read_mode).to_json()

    from ..models.user import User
    from datetime import date

    for item in input_data:
        first_name = item['FirstName']
        last_name = item['LastName']
        birth_date = item['BirthDate']
        registration_date = item['RegistrationDate']

        if birth_date.count('/') == 2 and registration_date.count('/') == 2:
            year, month, day = birth_date.split('/')
            try:
                year = int(year)
                month = int(month)
                day = int(day)
            except ValueError:
                WrongData('Birth date wrong')

            try:
                birth_date = date(year, month, day)
            except ValueError:
                WrongData('Birth date has wrong format')

            year, month, day = registration_date.split('/')
            try:
                year = int(year)
                month = int(month)
                day = int(day)
            except ValueError:
                WrongData('Registration date wrong')
            try:
                registration_date = date(year, month, day)
            except ValueError:
                WrongData('Registration date has wrong format')

            user = User(first_name, last_name, birth_date, registration_date)

            try:
                user.commit()
            except DataError:
                raise WrongData('All key cannot be nullable')
        else:
            raise WrongData('Date is not correct')
