class NotFoundTitles(Exception):
    pass


class UnknownReadMode(Exception):
    pass


class WrongData(Exception):
    pass


class CSVReader:
    def __init__(self, input_file, separators, necessarily_titles=None, read_mode='r'):
        self.file = self.read_file(input_file, read_mode)
        self.separator = ''
        self.separators = separators
        self.titles = self.init_titles(necessarily_titles)
        self.data = self.init_data()

    def read_file(self, input_file, read_mode):
        if read_mode == 'r':
            result_file = input_file.read()
        elif read_mode == 'rb':
            result_file = input_file.read().decode("utf-8")
        else:
            raise UnknownReadMode('Read mode for file is unknown')

        result_file = result_file.splitlines()

        return result_file

    def init_titles(self, necessarily_titles):
        titles = []
        for i, separator in enumerate(self.separators):
            titles = self.file[0].split(separator)
            if len(titles) > 1:
                self.separator = separator
                break

            if i == len(self.separators) - 1:
                raise WrongData('Wrong separators in file')

        if necessarily_titles:
            if len(titles) == len(necessarily_titles) and \
                    all(title in titles for title in necessarily_titles):
                return titles
            else:
                raise NotFoundTitles('File is not exist titles that it must have')
        else:
            return titles

    def init_data(self):
        result = []

        for line in self.file[1:]:
            split_line = line.split(self.separator)

            if len(split_line) == len(self.titles):
                result.append(split_line)
            else:
                raise WrongData(f'Number of data in line is wrong. '
                                      f'Expected {len(self.titles)}, but there are {len(split_line)}')

        return result

    def to_json(self):
        result = []

        for line in self.data:
            result.append({
                title: line[index] for index, title in enumerate(self.titles)
            })

        return result

