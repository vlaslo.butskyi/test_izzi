import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    SQLALCHEMY_DATABASE_URI = \
        f"sqlite:///{basedir}/db/db.sqlite3"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
