from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from .config import Config


def create_app():
    app = Flask(__name__, static_folder='static', template_folder='templates')

    app.config.from_object(Config)

    db = SQLAlchemy(app)
    migrate = Migrate(app, db)

    from .api import api_bp
    from .web import web_bp
    from .errors import errors_bp
    app.register_blueprint(api_bp, url_prefix='/api')
    app.register_blueprint(web_bp, url_prefix='/')
    app.register_blueprint(errors_bp)

    return app, db


app, db = create_app()

from .models.user import User
from .models.order import Order

# db.drop_all()
db.create_all()
