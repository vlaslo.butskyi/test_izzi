from . import api_bp


@api_bp.route('/')
def index():
    return 'Welcome!', 200


@api_bp.route('/users')
def users():
    from ..models.user import User

    import json

    users = User.query.all()

    json_result = []

    for user in users:
        json_result.append({
            'first_name': user.first_name,
            'last_name': user.last_name,
            'birth_date': user.birth_date.strftime("%d/%m/%Y"),
            'registration_date': user.registration_date.strftime("%d/%m/%Y"),
        })

    json_result = json.dumps(json_result)

    return json_result, 200


@api_bp.route('/users/<year>/<month>/<day>')
def users_by_date(year, month, day):
    from ..models.user import User

    from datetime import datetime
    import json

    search_day = datetime.strptime(f"{year}.{month}.{day}", "%Y.%m.%d")

    users = User.query.filter(User.registration_date == search_day)

    json_result = []

    for user in users:
        json_result.append({
            'first_name': user.first_name,
            'last_name': user.last_name,
            'birth_date': user.birth_date.strftime("%m/%d/%Y"),
            'registration_date': user.registration_date.strftime("%m/%d/%Y"),
        })

    json_result = json.dumps(json_result)

    return json_result, 200
